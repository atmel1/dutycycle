/**
 * \file: icp.h
 *
 * \brief AVR135: Using Timer Capture to Measure PWM Duty Cycle
 *
 * Copyright (C) 2016 Atmel Corporation. All rights reserved.
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
 
#if	!defined(_AVR135_ICP_H_)
#define _AVR135_ICP_H_	1

/*
 * LED debug definitions.
 * The current detected PWD duty cycle is displayed on PC[7:0]
 */

/* assume analog data		*/
#define	ICP_ANALOG		1					

/* queue only 1 element		*/
#define	ICP_BUFSIZE		1					

#if		ICP_ANALOG
	/* same as BUFSIZE			*/
	#define	ICP_RX_QSIZE	ICP_BUFSIZE		
#else	/* ICP_DIGITAL */
	/* 1 extra for queue management */
	#define	ICP_RX_QSIZE	(ICP_BUFSIZE+1)	
#endif


#define	ICP_SCALE	256U
#if		ICP_SCALE <= 256
typedef	unsigned char icp_sample_t;
typedef	unsigned int  icp_total_t;
#else
typedef	unsigned int icp_sample_t;
typedef	unsigned long  icp_total_t;
#endif

/*
 * API Prototypes
 */
void icp_init(void);
icp_sample_t icp_rx(void);

#endif	/* AVR135_ICP_H_ */
