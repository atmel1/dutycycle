/**
 * \file: main.c
 *
 * \brief AVR135: Using Timer Capture to Measure PWM Duty Cycle
 *
 * Copyright (C) 2016 Atmel Corporation. All rights reserved.
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include "icp.h"
#include "device.h"

/* counts [0:4] to make the test visible */
static volatile unsigned char hb_count = 0;			



/**
 * TIMER0_OVF()
 *
 * Overflow ISR for timer0.
 * Updates the PWM duty cycle (timer2).
 */
ISR(TIMER0_OVF_vect)
{
	if (++hb_count >= 32)	/* approx 0.25 seconds */
	{
		/*
		 * Clear the post-scale counter
		 */
		hb_count = 0;

		/*
		 * Update the duty cycle (OCR2). The result is allowed
		 * to overflow back to 0 to restart the run.
		 */
		REG_DUTYCYCLE += 1;
	}
}

/**
 * hb_init()
 *
 * Start up heartbeat timer.
 *
 * It updates the duty cycle for the PWM train being Demodulated.
 * Using the internal 8MHz clock, and a /1024 prescale, the
 * (8-bit) timer overflows at about 32Hz. A counter [0:31) in the ISR
 * slows this down by 32, so the duty cycle changes approx. 1Hz.
 * it just needs to be slow enough to be visible on the LEDs.
 */
void hb_init(void)
{
	/*
	 * The heartbeat timer (timer0) is used in free-running mode.
	 */
	/* disable asynchronous mode */
	REG_ASYNC_STATUS = 0;						
	/*EMPTY*/
	while (REG_ASYNC_STATUS) ;					
	/* prescale /1024 */
	REG_TC0_CONTROL = REG_TC0_CLKSEL;			
	/* enable overflow interrupt */
	REG_TC0_INTMASK = MSK_TC0_INTENABLE;		
}

/**
 * pwm_init()
 *
 * Start up PWM (timer2) for demonstration purposes.
 * It drives a non-inverted PWM train, which will be
 * Demodulated by the ICP1 ISR (icp.c).
 * For connections refer the application note or device.h.
 */
void pwm_init(void)
{
	/*
	 * Set up PWM using timer2
	 */
	/* enable PWM output pin */
	PORT_PWM_OUTPUT |= MSK_PWM_OUTPUT;		
	/* start at 0% */
	REG_DUTYCYCLE = 0;				
	/* Configure TC2 */
	REG_TC2_CONTROLA = MSK_TC2_CONTROLA;
	REG_TC2_CONTROLB = MSK_TC2_CONTROLB;

}

/**
 * main()
 *
 * Demonstration main program.
 * 
 */
int main(void)
{
	icp_sample_t sample;
	/*
	 * Init subsystems
	 */
	/* main.c	*/
	hb_init();						
	/* icp.c	*/
	icp_init();						
	/* main.c	*/
	pwm_init();						
	
	/*
	 * PORTC for LED debug
	 */
	/* initially off */
	LED_DEBUG_PORT = 0xFF;			
	/* all output */
	LED_DEBUG_DDR = 0xFF;			
	
	/*
	 * The ISRs do most of the work.
	 */
	/* enable interrupts since init is done */
	sei();							

	/* enable (idle mode) sleep */
	sleep_enable();					
	
	/*
	 * Loop forever
	 */
	for (;;)
	{
		/*
		 * Fetch the latest reading and display it on the LEDs.
		 */
		sample = icp_rx();
		
		LED_DEBUG_PORT = ~sample;

		/*
		 * Sleep until the next interrupt. This will wake up twice
		 * per PWM period, plus (approx.) once per second for the
		 * heartbeat/update timer. This is more often than needed,
		 * but is certainly sufficient for this demonstration.
		 */
		sleep_cpu();
	}
	/*NOTREACHED*/
	return(0);

}

