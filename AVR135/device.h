/**
 * \file: device.h
 *
 * \brief AVR135: Using Timer Capture to Measure PWM Duty Cycle
 *
 * Copyright (C) 2016 Atmel Corporation. All rights reserved.
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/"> 
 * Atmel Support</a>
 */
 
#include <avr/io.h>

/*
 * Device selection made in the project takes care of appropriate definitions 
 * needed for the project.
 * Atmel Studio 7 (Version: 7.0.790) � 2015 Atmel Corp. All rights reserved.
 *
 */
/* For ATmega328PB device */
#if defined (_AVR_ATMEGA328PB_H_INCLUDED)		
	#define DEVICE_ATMEGA328PB
/* For ATmega64 device */
#elif defined (_AVR_IOM64_H_)					
	#define DEVICE_ATMEGA64
/* Invalid device selection */
#else											
	#error "Device selected has to be either ATmega64 or ATmega328PB."
	#define DEVICE_ATTINY20
#endif

/*
 * Definitions needed for ATmega64
 */
#ifdef DEVICE_ATMEGA64

/*
 * Definitions for the ICP pin; for this example we use timer 1
 */
#define	ICP_PIN				PIND			/* ICP1 GPIO value	*/
#define	ICP_PORT			PORTD			/* ICP1 GPIO port	*/
#define	ICP_DDR				DDRD			/* ICP1 GPIO DDR	*/
#define	ICP_BIT				PD4				/* ICP1 GPIO pin	*/

/*
 * Definitions for ICP timer (1) setup.
 */
#define	ICP_OCR				OCR1A		 /* ICP1 Output Compare register */
#define	ICP_OC_IE			OCIE1A		 /* ICP1 timer Output Compare enable */
#define	ICP_OC_IF			OCF1A		 /* ICP1 timer Output Compare flag */
#define	ICP_IE				TICIE1		 /* ICP1 interrupt enable */
#define	ICP_IF				ICF1		 /* ICP1 interrupt flag	*/
#define	ICP_CTL_A			TCCR1A		 /* ICP1 timer control */
#define	ICP_CTL				TCCR1B		 /* ICP1 interrupt control */
/* Input Capture and the Output Capture interrupts */
#define	ICP_TIMSK			TIMSK      
/* Input Capture and the Output Capture interrupts */
#define	ICP_TIMSK_MASK		(1 << ICP_IE) | (1 << ICP_OC_IE) 
#define	ICP_SENSE			ICES1	/* ICP1 interrupt sense (rising/falling) */
/* prescale /1 */
#define	ICP_PRESCALE 		((0 << CS12) | (0 << CS11) | (1 << CS10))	
#define REG_TIMER_INTFLAG   TIFR 		   /* TC1 Interrupt Flag Register */
#define	ICP_START_SENSE		(1 << ICP_SENSE)	/* start with rising edge */

/*
 * Definitions for the ICP pin; for this example we use timer 1
 */
#define	LED_DEBUG_DDR		DDRC		 /* LED Output GPIO DDR */
#define	LED_DEBUG_PORT		PORTC		 /* LED Output GPIO PORT */
#define REG_DUTYCYCLE		OCR2		 /* ICP1 Output Compare register */
#define REG_ASYNC_STATUS	ASSR		 /* Asynchronous Status Register */
#define REG_TC0_CONTROL		TCCR0		 /* TC0 Control Register */
#define REG_TC0_INTMASK		TIMSK	 /* TC0 Control Interrupt Mask Register */
/* TC0 Control Interrupt Enable Mask */
#define MSK_TC0_INTENABLE	(1 << TOIE0) 
/* TC0 Clock Enable Register */
#define REG_TC0_CLKSEL		((1 << CS02) | (1 << CS01) | (1 << CS00)) 

#define PORT_PWM_OUTPUT		DDRB		 /* PWM GPIO DDR */
#define MSK_PWM_OUTPUT		(1 << PB7)	 /* PWM GPIO Pin */

#define REG_TC2_CONTROLA	TCCR2		 /* TC2 Control Register */
/* TC2 Control Mask */
#define MSK_TC2_CONTROLA	(MSK_TC2_FAST_PWM | MSK_TC2_NONINV_PWM | MSK_TC2_PRESCALE)
#define REG_TC2_CONTROLB	TCCR2		 /* Dummy Control Register for mega64 */
#define MSK_TC2_CONTROLB	TCCR2		 /* Dummy Control Register Mask for mega64 */
#define MSK_TC2_FAST_PWM	((1 << WGM21) | (1 << WGM20))  /* TC2 PWM Mask */
/* TC2 Compare Output Mask */
#define MSK_TC2_NONINV_PWM	((1 << COM21) | (0 << COM20))  
/* TC2 Clock Prescale */
#define MSK_TC2_PRESCALE	((0 << CS22)  | (1 << CS21) | (1 << CS20))  

#endif

#ifdef DEVICE_ATMEGA328PB

/*
 * Definitions for the ICP pin; for this example we use timer 1
 */
#define	ICP_PIN				PINB			/* ICP1 GPIO value	*/
#define	ICP_PORT			PORTB			/* ICP1 GPIO port	*/
#define	ICP_DDR				DDRB			/* ICP1 GPIO DDR	*/
#define	ICP_BIT				PB0				/* ICP1 GPIO pin	*/

/*
 * Definitions for ICP timer (1) setup.
 */
#define	ICP_OCR				OCR1A		 /* ICP1 Output Compare register */
#define	ICP_OC_IE			OCIE1A		 /* ICP1 timer Output Compare enable */
#define	ICP_OC_IF			OCF1A		 /* ICP1 timer Output Compare flag */
#define	ICP_IE				ICIE1		 /* ICP1 interrupt enable */
#define	ICP_IF				ICF1		 /* ICP1 interrupt flag	*/
#define	ICP_CTL_A			TCCR1A		 /* ICP1 timer control */
#define	ICP_CTL				TCCR1B		 /* ICP1 interrupt control */
/* Input Capture and the Output Capture interrupts */
#define	ICP_TIMSK			TIMSK1
/* Input Capture and the Output Capture interrupts */
#define	ICP_TIMSK_MASK		(1 << ICP_IE) | (1 << ICP_OC_IE)
/* ICP1 interrupt sense (rising/falling) */
#define	ICP_SENSE			ICES1			
/* prescale /1 */
#define	ICP_PRESCALE 		((0 << CS12) | (0 << CS11) | (1 << CS10))	
#define REG_TIMER_INTFLAG   TIFR1			/* TC1 Interrupt Flag Register */
#define	ICP_START_SENSE		(1 << ICP_SENSE)	/* start with rising edge	*/


#define	LED_DEBUG_DDR		DDRD			/* LED Output GPIO DDR */
#define	LED_DEBUG_PORT		PORTD			/* LED Output GPIO PORT */
#define REG_DUTYCYCLE		OCR2A			/* ICP1 Output Compare register */
#define REG_ASYNC_STATUS	ASSR			/* Asyncronous Status Register */
#define REG_TC0_CONTROL		TCCR0B			/* TC0 Control Register */
/* TC0 Control Interrupt Mask Register */
#define REG_TC0_INTMASK		TIMSK0
/* TC0 Control Interrupt Enable Mask */
#define MSK_TC0_INTENABLE	(1 << TOIE0)
/* TC0 Clock Enable Register */
#define REG_TC0_CLKSEL		((1 << CS02) | (0 << CS01) | (1 << CS00))
#define PORT_PWM_OUTPUT		DDRB			/* PWM GPIO DDR */
#define MSK_PWM_OUTPUT		(1 << DDRB3)	/* PWM GPIO Pin */

#define REG_TC2_CONTROLA	TCCR2A			/* TC2 Control Register */
/* TC2 Control Mask */
#define MSK_TC2_CONTROLA	(MSK_TC2_FAST_PWM | MSK_TC2_NONINV_PWM) 
#define REG_TC2_CONTROLB	TCCR2B					/* TC2 Control Register */
#define MSK_TC2_CONTROLB	MSK_TC2_PRESCALE		/* TC2 Control Mask */
#define MSK_TC2_FAST_PWM	((1 << WGM21) | (1 << WGM20))  /* TC2 PWM Mask */
/* TC2 Compare Output Mask */
#define MSK_TC2_NONINV_PWM	((1 << COM2A1) | (0 << COM2A0))  
/* TC2 Clock Prescale */
#define MSK_TC2_PRESCALE	((0 << CS22)  | (1 << CS21) | (1 << CS20)) 

#endif

#ifdef DEVICE_ATTINY20

/*
 * Definitions for the ICP pin; for this example we use timer 1
 */
#define	ICP_PIN				PINA			/* ICP1 GPIO value	*/
#define	ICP_PORT			PORTA			/* ICP1 GPIO port	*/
#define	ICP_DDR				DDRA			/* ICP1 GPIO DDR	*/
#define	ICP_BIT				PORTA0			/* ICP1 GPIO pin	*/

/*
 * Definitions for ICP timer (1) setup.
 */
#define	ICP_OCR				OCR1A		 /* ICP1 Output Compare register */
#define	ICP_OC_IE			OCIE1A		 /* ICP1 timer Output Compare enable */
#define	ICP_OC_IF			OCF1A		 /* ICP1 timer Output Compare flag */
#define	ICP_IE				ICIE1		 /* ICP1 interrupt enable */
#define	ICP_IF				ICF1		 /* ICP1 interrupt flag	*/
#define	ICP_CTL_A			TCCR1A		 /* ICP1 timer control */
#define	ICP_CTL				TCCR1B		 /* ICP1 interrupt control */
/* Input Capture and the Output Capture interrupts */
#define	ICP_TIMSK			TIMSK
/* Input Capture and the Output Capture interrupts */
#define	ICP_TIMSK_MASK		(1 << ICP_IE) | (1 << ICP_OC_IE)
/* ICP1 interrupt sense (rising/falling) */
#define	ICP_SENSE			ICES1			
/* prescale /1 */
#define	ICP_PRESCALE 		((0 << CS12) | (0 << CS11) | (1 << CS10))	
#define REG_TIMER_INTFLAG   TIFR			/* TC1 Interrupt Flag Register */
#define	ICP_START_SENSE		(1 << ICP_SENSE)	/* start with rising edge	*/


#define	LED_DEBUG_DDR		DDRB			/* LED Output GPIO DDR */
#define	LED_DEBUG_PORT		PORTB			/* LED Output GPIO PORT */
#define REG_DUTYCYCLE		OCRA			/* ICP1 Output Compare register */
#define REG_ASYNC_STATUS	ASSR			/* Asyncronous Status Register */
#define REG_TC0_CONTROL		TCCR0B			/* TC0 Control Register */
/* TC0 Control Interrupt Mask Register */
#define REG_TC0_INTMASK		TIMSK
/* TC0 Control Interrupt Enable Mask */
#define MSK_TC0_INTENABLE	(1 << TOIE0)
/* TC0 Clock Enable Register */
#define REG_TC0_CLKSEL		((1 << CS02) | (0 << CS01) | (1 << CS00))
#define PORT_PWM_OUTPUT		DDRB			/* PWM GPIO DDR */
#define MSK_PWM_OUTPUT		(1 << DDRB3)	/* PWM GPIO Pin */

#define REG_TC2_CONTROLA	TCCR2A			/* TC2 Control Register */
/* TC2 Control Mask */
#define MSK_TC2_CONTROLA	(MSK_TC2_FAST_PWM | MSK_TC2_NONINV_PWM) 
#define REG_TC2_CONTROLB	TCCR2B					/* TC2 Control Register */
#define MSK_TC2_CONTROLB	MSK_TC2_PRESCALE		/* TC2 Control Mask */
#define MSK_TC2_FAST_PWM	((1 << WGM21) | (1 << WGM20))  /* TC2 PWM Mask */
/* TC2 Compare Output Mask */
#define MSK_TC2_NONINV_PWM	((1 << COM2A1) | (0 << COM2A0))  
/* TC2 Clock Prescale */
#define MSK_TC2_PRESCALE	((0 << CS22)  | (1 << CS21) | (1 << CS20)) 

#endif
